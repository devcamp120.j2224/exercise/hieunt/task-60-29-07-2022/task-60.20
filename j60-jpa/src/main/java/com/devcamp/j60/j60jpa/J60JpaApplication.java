package com.devcamp.j60.j60jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J60JpaApplication {

	public static void main(String[] args) {
		SpringApplication.run(J60JpaApplication.class, args);
	}

}
