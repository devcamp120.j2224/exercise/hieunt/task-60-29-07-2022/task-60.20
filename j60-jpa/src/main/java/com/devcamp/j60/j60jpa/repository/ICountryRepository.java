package com.devcamp.j60.j60jpa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j60.j60jpa.model.CCountry;

public interface ICountryRepository extends JpaRepository<CCountry, Long> {
	CCountry findByCountryCode(String countryCode);
}
