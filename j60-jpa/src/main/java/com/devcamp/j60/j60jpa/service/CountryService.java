package com.devcamp.j60.j60jpa.service;

import java.util.ArrayList;
import java.util.Set;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.j60.j60jpa.model.CCountry;
import com.devcamp.j60.j60jpa.model.CRegion;
import com.devcamp.j60.j60jpa.repository.ICountryRepository;

@Service
public class CountryService {
	@Autowired
	ICountryRepository pCountryRepository;
    
    public ArrayList<CCountry> getAllCountries() {
        ArrayList<CCountry> listCountry = new ArrayList<>();
        pCountryRepository.findAll().forEach(listCountry::add);
        return listCountry;
    }
    public Set<CRegion> getRegionsByCountryCode(String countryCode) { 
        /*
        List<CRegion> pRegions = new ArrayList<CRegion>();
		pRegionRepository.findAll().forEach(pRegions::add);
        */
        CCountry vCountry = pCountryRepository.findByCountryCode(countryCode);
        if (vCountry != null) {
            return  vCountry.getRegions();
        } else {
            return null;
        }
    }
}