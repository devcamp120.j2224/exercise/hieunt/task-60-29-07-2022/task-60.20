package com.devcamp.j60.j60jpa.service;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import com.devcamp.j60.j60jpa.model.CRegion;
import com.devcamp.j60.j60jpa.repository.IRegionRepository;

import org.springframework.stereotype.Service;

@Service
public class RegionService {
    @Autowired
	IRegionRepository pRegionRepository;

    public ArrayList<CRegion> getAllRegions() {
        ArrayList<CRegion> regions = new ArrayList<>();        
        //List<CRegion> pRegions = new ArrayList<CRegion>();
		pRegionRepository.findAll().forEach(regions::add);
        return regions;
    }
   
}